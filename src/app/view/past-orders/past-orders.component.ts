import { Component, OnInit } from "@angular/core";
import { IInvoiceItemComposite } from "src/app/services/invoice-item-composite/invoice-item-composite";
import { ISalesOrder } from 'src/app/services/sales-order/sales-order'
import { SalesOrderService } from "src/app/services/sales-order/sales-order.service";
import { UserService } from "src/app/services/user/user.service";

@Component({
  selector: 'app-past-orders',
  templateUrl: './past-orders.component.html',
  styleUrls: ['./past-orders.component.css']
})
export class PastOrdersComponent implements OnInit {
  salesOrders:ISalesOrder[] = [];
  cId = 0;

  constructor(private salesOrderService: SalesOrderService, private userServ: UserService){ }
  ngOnInit(): void {
    this.userServ.getCurrentUser(localStorage.getItem('currentUser')).subscribe(
      response => {
        this.cId = response.customerId;
        this.getAllOrders(this.customerId);
      }
    ); 
  }
  getAllOrders(customerId:number){
    this.salesOrderService.getSalesOrder(customerId).subscribe(
      response => {
        for(let dbSalesOrder of response){
          let totalPrice:number = 0;
          let composites:IInvoiceItemComposite[] = [];
          for(let dbInvoiceItem of dbSalesOrder.itemList){
            let inventoryPrice:number = dbInvoiceItem.inventory.salesPrice;
            totalPrice += inventoryPrice * dbInvoiceItem.quantity;

            let composite:IInvoiceItemComposite = {
              inventoryProductName: dbInvoiceItem.inventory.productName,
              inventoryImage: dbInvoiceItem.inventory.menuImg,
              invoiceQuantity: dbInvoiceItem.quantity
            }
            composites.push(composite);
          }
          
          let salesOrder:ISalesOrder = {
            orderDate: "",
            orderId: dbSalesOrder.orderId,
            invoiceItems: composites,
            totalPrice: totalPrice
          }
          this.salesOrders.push(salesOrder);
        }
      }
    )
  }
  get customerId(): number { return this.cId; }
  set customerId(temp: number) { this.cId = temp; }
}
