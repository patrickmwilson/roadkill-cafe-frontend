import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service';
import { RouterTestingModule } from '@angular/router/testing'
import { ChangeInfoComponent } from './change-info.component';
import { HttpClient } from '@angular/common/http';
import { By } from '@angular/platform-browser';

describe('ChangeInfoComponent', () => {

  class MockService{
    updateUser(){}
    getCurrentUser(){}
  }

  let dummyUserData = {
    customerId: 1,
    username: 'testuser',
    password: 'testpass',
    email: 'test@test.com',
    firstName: 'testfname', 
    lastName: 'testlastname',
    phoneNumber: '0001110000'
  }

  let router: Router;
  let component: ChangeInfoComponent;
  let fixture: ComponentFixture<ChangeInfoComponent>;
  let userService: UserService;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy}

  // beforeEach(async () => {
  //   await TestBed.configureTestingModule({
  //     declarations: [ ChangeInfoComponent ]
  //   })
  //   .compileComponents();
  // });

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
      ],
      declarations: [ ChangeInfoComponent ],
      providers:[
        {provide: UserService, useClass: MockService},
        {provide: HttpClient, useValue: mockClient}
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ChangeInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.inject(Router);
    userService = TestBed.inject(UserService);
    mockClient = TestBed.get(HttpClient);
  });
  
  it('should create', waitForAsync(()=>{

    
    spyOn(component, 'ngOnInit');
    component.ngOnInit();
    
    fixture.whenStable().then(()=>{
      expect(component).toBeTruthy();
    });

  }));

  it('should call the postChangeInfo() method', waitForAsync(()=>{

    let submitButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;
    spyOn(component, 'postChangeInfo');
    submitButton.click();
    
    fixture.whenStable().then(()=>{
      expect(component.postChangeInfo).toHaveBeenCalled();
    });

  }));

});
