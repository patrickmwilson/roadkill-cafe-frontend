import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { UserService } from '../services/user/user.service';
import { RouterTestingModule } from '@angular/router/testing';
import { LoginComponent } from './login.component';
import { HttpClient } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { Observable } from 'rxjs';
import { Location } from '@angular/common';
import { WelcomeComponent } from '../welcome/welcome.component';

describe('LoginComponent', () => {


  class MockService {
    login(){}
  }

  let dummyUserData = {
    customerId: 1,
    username: 'testuser',
    password: 'testpass',
    email: 'test@test.com',
    firstName: 'testfname', 
    lastName: 'testlastname',
    phoneNumber: '0001110000'
  }

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let router: Router;
  let location: Location;
  let userService: UserService;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy};

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: '', component: LoginComponent},
          {path: 'welcome', component: WelcomeComponent}
        ]),
        FormsModule,
      ],
      declarations: [ LoginComponent, WelcomeComponent ],
      providers: [
        {provide: UserService, useClass: MockService},
        {provide: HttpClient, useValue: mockClient},
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(LoginComponent);
    component = fixture.componentInstance;


    fixture.detectChanges();
    component.username = dummyUserData.username;
    component.password = dummyUserData.password;

    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    userService = TestBed.inject(UserService);
    mockClient = TestBed.get(HttpClient);
    
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have h3 say Login', ()=>{
    fixture.detectChanges();
    let head = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(head.innerHTML).toBe('Login');
  });

  it('should call the postLogin() method', waitForAsync(()=>{
    fixture.detectChanges();
    let loginButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;
    spyOn(component, 'postLogin');
    loginButton.click();

    fixture.whenStable().then(()=>{
      expect(component.postLogin).toHaveBeenCalled();
    });
  }));

  it('should redirect to welcome component after calling the login() method async', waitForAsync(()=>{
    fixture.detectChanges();

    let userServiceSpy = spyOn(userService, 'login').and.returnValue(Observable.create(observer=>{
      observer.next("asd");
    }));

    spyOn(component, 'windowReload').call({});

    let loginButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;
    
    loginButton.click();

    fixture.detectChanges();

    fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/welcome');
    });
  }));
});
