import { CommonModule } from '@angular/common';
import { Component } from '@angular/core';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import {Location} from '@angular/common';
import { WelcomeComponent } from './welcome.component';
import { AboutUsComponent } from '../about-us/about-us.component';
import { MenuComponent } from '../menu/menu.component';
import { CartComponent } from '../cart/cart.component';

@Component({
  template: '<a routerLink="/aboutUs" id= "about-us" aboutclass="btn btn-primary" role="button" aria-pressed="true">About Us</a>'
})
class DummyComponent {
}

describe('WelcomeComponent', () => {

  let router: Router;
  let location: Location;
  let component: WelcomeComponent;
  let fixture: ComponentFixture<WelcomeComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        CommonModule,
        RouterTestingModule.withRoutes([
          {path: '', component: WelcomeComponent},
          {path: 'about-us', component: AboutUsComponent},
          {path: 'menu', component: MenuComponent},
          {path: 'cart', component: CartComponent},
        ]),
      ],
      declarations: [WelcomeComponent, DummyComponent ],
    }).compileComponents();
    fixture = TestBed.createComponent(WelcomeComponent);
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    component = fixture.componentInstance;
    component.username = 'testUser';
    fixture.detectChanges();
    // router.navigateByUrl('/about-us');
  });


  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have h1 say Welcome to Roadkill Cafe Catering!', () => {
    fixture.detectChanges();
    let welcomeHead = fixture.debugElement.query(By.css('h1')).nativeElement;
    expect(welcomeHead.innerHTML).toBe('Welcome to Roadkill Cafe Catering!');

  });

  it('should go to About Us page',  waitForAsync(()=>{
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#about-us')).nativeElement.click();

    fixture.whenStable().then(()=>{
        expect(location.path()).toEqual('/about-us');
      });
    
}));

it('should go to Menu page',  waitForAsync(()=>{
  fixture.detectChanges();
  fixture.debugElement.query(By.css('#menu')).nativeElement.click();

  fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/menu');
    });
  
}));

it('should go to Cart page',  waitForAsync(()=>{
  fixture.detectChanges();
  fixture.debugElement.query(By.css('#cart')).nativeElement.click();

  fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/cart');
    });
  
}));

});
 