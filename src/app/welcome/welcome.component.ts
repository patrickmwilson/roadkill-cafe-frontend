import { NumberSymbol } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  [x: string]: any;


  username = localStorage.getItem("currentUser");
  constructor(private router: Router) {}
  navAboutUs(){
    this.router.navigate(['/about-us']);
  }
  navMenu(){

  }
  

  ngOnInit(): void { 
  }

}
 