import { TestBed } from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing'
import { CartService } from './cart.service';
import { UserService } from './user/user.service';

const dummyAddToCartData = {
  customerId: 1,
  itemId: 1,
  quantity: 1
};

const dummyAddToCartResponse = "Resource created!";

describe('CartService', () => {
  let service: CartService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[HttpClientTestingModule],
      providers:[UserService],
    });
    service = TestBed.inject(CartService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  afterEach(()=>{
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have addToCart(customerId:number, itemId:number, quantity:number) return a response string', ()=>{
    service.addToCart(1, 1, 1).subscribe(
      response => {
        expect(response.toString()).toEqual(dummyAddToCartResponse);
      }
    );

    const req = httpMock.expectOne("http://localhost:9025/salesorders/add");
    expect(req.request.method).toBe('POST');
    req.flush(dummyAddToCartResponse);
  });

  it('should have getPendingOrder(customerId: number) return data', ()=>{
    service.getPendingOrder(1).subscribe(
      response => {
        expect(response.toString()).toEqual("asd");
      }
    );

    const req = httpMock.expectOne("http://localhost:9025/salesorders/pending/1");
    expect(req.request.method).toBe('GET');
    req.flush("asd");
  });

  it('should have updateQuantity(invoiceItemId: number, quantity: number) return a status string', ()=>{
    service.updateQuantity(1,1).subscribe(
      response => {
        expect(response.toString()).toEqual("asd");
      }
    );

    const req = httpMock.expectOne("http://localhost:9025/invoiceitems/update");
    expect(req.request.method).toBe('POST');
    req.flush("asd");
  });

  it('should have submitOrder(customerId: number) return a status string', ()=>{
    service.submitOrder(1).subscribe(
      response => {
        expect(response.toString()).toEqual("asd");
      }
    );
    const req = httpMock.expectOne("http://localhost:9025/salesorders/submit");
    expect(req.request.method).toBe('POST');
    req.flush("asd");
  });
});
