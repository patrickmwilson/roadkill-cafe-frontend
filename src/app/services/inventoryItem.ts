export class InventoryItem {
    itemId: number;
    productName: string;
    salesPrice: number;
    description: string;
    menuImg: string;
    quantity: number;
    invoiceItemId: number;
    totalPrice: number;

    constructor(itemId: number, productName: string, salesPrice: number, description: string, menuImg: string) {
        this.itemId = itemId;
        this.productName = productName;
        this.salesPrice = salesPrice;
        this.description = description;
        this.menuImg = menuImg;
        this.quantity = 0;
    }

    increment() {
        this.quantity++;
    }

    decrement() {
        if(this.quantity <= 0) {
            this.quantity = 0;
        } else {
            this.quantity--;
        }
    }
}