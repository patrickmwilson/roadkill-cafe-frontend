import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class CartService {

  constructor(private httpCli: HttpClient) { } 

  baseURL: string = "http://ec2-3-23-20-124.us-east-2.compute.amazonaws.com:9025/";


  addToCart(customerId: number, itemId: number, quantity: number): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = { "customerId": customerId,
                  "itemId": itemId,
                  "quantity": quantity};

    return this.httpCli.post(this.baseURL + "salesorders/add", body, {"headers": headers, responseType: "text"});
  }

  getPendingOrder(customerId: number): Observable<any> {
    return this.httpCli.get<any>(this.baseURL + "salesorders/pending/" + customerId);
  }

  updateQuantity(invoiceItemId: number, quantity: number): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = { "invoiceItemId": invoiceItemId,
                  "quantity": quantity};
    return this.httpCli.post(this.baseURL + "invoiceitems/update", body, {"headers": headers, responseType: "text"});
  }

  submitOrder(customerId: number): Observable<any> {
    var headers = { 'content-type': 'application/json'};
    var body = { "customerId": customerId};

    return this.httpCli.post(this.baseURL + "salesorders/submit", body, {"headers": headers, responseType: "text"});
  }
}
