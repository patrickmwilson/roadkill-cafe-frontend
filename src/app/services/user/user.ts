export class User {
    customerId: number;
    username: string;
    password: string;
    email: string;
    firstName: string; 
    lastName: string;
    phoneNumber: string;
}