import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SalesOrderService {
    baseURL: string = "http://ec2-3-23-20-124.us-east-2.compute.amazonaws.com:9025/";

  constructor(private httpCli: HttpClient) { }

  getSalesOrder(customerId:number): Observable<any> {
    return this.httpCli.get<any>(this.baseURL + 'salesorders/' + customerId);
  }
}