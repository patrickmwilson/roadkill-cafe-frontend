import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CartService } from '../services/cart.service';
import { UserService } from '../services/user/user.service';
import { InventoryItem } from '../services/inventoryItem';

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.css']
})
export class CheckoutComponent implements OnInit {

  constructor(private cartServ: CartService, private router: Router, private userServ: UserService) { }

  cId = 0;

  get customerId(): number {
    return this.cId;
  }

  set customerId(temp: number) {
    this.cId = temp;
  }

  get numItems(): number {
    return this.cartItems.length;
  }

  total = 0;

  get totalPrice(): number {
    return this.total;
  }

  cartItems: InventoryItem[] = [];

  submitOrder() {
    this.cartServ.submitOrder(this.customerId).subscribe(
      response => {
        console.log(response);
        this.router.navigate(['/order-success'])
          .then(()=>{
            window.location.reload();
          });
      }
    );
  }

  getPendingOrder(custId: number) {
    this.cartServ.getPendingOrder(custId).subscribe(
      response => {
        //var total: number = 0;
        for(var temp of response.itemList) {
          var inventory = temp.inventory;
          var item = new InventoryItem(inventory.itemId, inventory.productName, inventory.salesPrice, inventory.description, inventory.menuImg);
          item.quantity = temp.quantity;
          item.invoiceItemId = temp.invoiceItemId;
          item.totalPrice = item.salesPrice * item.quantity;
          this.total += item.totalPrice;
          this.cartItems.push(item);
        }
      }
    );
  }

  ngOnInit(): void {
    this.userServ.getCurrentUser(localStorage.getItem('currentUser')).subscribe(
      response => {
        this.cId = response.customerId;
        this.getPendingOrder(response.customerId);
      }
    );
  }

}
