import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { RouterModule } from '@angular/router';
import { MenuComponent } from './menu/menu.component';
import { LoginComponent } from './login/login.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { RegisterAccountComponent } from './register-account/register-account.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { CartComponent } from './cart/cart.component';
import { ChangeInfoComponent } from './change-info/change-info.component';
import { UserPortalComponent } from './user-portal/user-portal.component';
import { PastOrdersComponent } from './view/past-orders/past-orders.component';
import { CheckoutComponent } from './checkout/checkout.component';
import { OrderSuccessComponent } from './order-success/order-success.component';


@NgModule({
  declarations: [
    AppComponent,
    WelcomeComponent,
    MenuComponent,
    LoginComponent,
    ForgotPasswordComponent,
    RegisterAccountComponent,
    AboutUsComponent,
    CartComponent,
    ChangeInfoComponent,
    UserPortalComponent,
    PastOrdersComponent,
    CheckoutComponent,
    OrderSuccessComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    FontAwesomeModule,
    RouterModule.forRoot([
      {path: 'welcome', component: WelcomeComponent},
      {path: 'menu', component: MenuComponent},
      {path: 'login', component: LoginComponent},
      {path: 'register-account', component: RegisterAccountComponent},
      {path: 'forgot-password', component: ForgotPasswordComponent},
      {path: 'about-us', component: AboutUsComponent},
      {path: 'cart', component: CartComponent},
      {path: 'change-info', component: ChangeInfoComponent},
      {path: 'user-portal', component: UserPortalComponent},
      {path: 'orders', component: PastOrdersComponent},
      {path: 'checkout', component: CheckoutComponent},
      {path: 'order-success', component: OrderSuccessComponent},
      {path: 'past-orders', component: PastOrdersComponent},
      {path: '*', component: WelcomeComponent}
    ])
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
