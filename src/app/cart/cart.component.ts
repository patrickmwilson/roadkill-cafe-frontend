import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { CartService } from '../services/cart.service';
import { InventoryItem } from '../services/inventoryItem';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  constructor(private cartServ: CartService, private userServ: UserService, private router: Router) { }
  username = localStorage.getItem('currentUser');
  faMinus = faMinusSquare;
  faPlus = faPlusSquare;

  cId = 0;

  get customerId(): number {
    return this.cId;
  }

  set customerId(temp: number) {
    this.cId = temp;
  }

  cartItems: InventoryItem[] = [];

  updateInvoiceItem(invoiceItemId: number, quantity: number) {
    this.cartServ.updateQuantity(invoiceItemId, quantity).subscribe(
      response => {
        console.log(response);
      }
    );
    location.reload();
  }

  getPendingOrder(custId: number) {
    this.cartServ.getPendingOrder(custId).subscribe(
      response => {
        for(var temp of response.itemList) {
          var inventory = temp.inventory;
          var item = new InventoryItem(inventory.itemId, inventory.productName, inventory.salesPrice, inventory.description, inventory.menuImg);
          item.quantity = temp.quantity;
          item.invoiceItemId = temp.invoiceItemId;
          this.cartItems.push(item);
        }
      }
    );
  }

  ngOnInit(): void {
    this.userServ.getCurrentUser(localStorage.getItem('currentUser')).subscribe(
      response => {
        this.cId = response.customerId;
        this.getPendingOrder(response.customerId);
      }
    );

    
  }

}
