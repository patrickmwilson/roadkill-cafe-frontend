import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { faMinusSquare, faPlusSquare } from '@fortawesome/free-solid-svg-icons';
import { CartService } from '../services/cart.service';
import { InventoryItemService } from '../services/inventory-item.service';
import { InventoryItem } from '../services/inventoryItem';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.css']
})
export class MenuComponent implements OnInit {

  constructor(private inventoryItemServ: InventoryItemService, private userServ: UserService, private cartServ: CartService, private router: Router){ }

  pageTitle= "Menu";
  faMinus = faMinusSquare;
  faPlus = faPlusSquare;

  inventoryItems: InventoryItem[] = [];

  cId = 0;

  get customerId(): number {
    return this.cId;
  }

  set customerId(temp: number) {
    this.cId = temp;
  }
  
  getMenuItems() {
    this.inventoryItemServ.getAllItems().subscribe(
      response => {
        for(var temp of response) {
          this.inventoryItems.push(new InventoryItem(temp.itemId, temp.productName, temp.salesPrice, temp.description, temp.menuImg));
        }
      }
    );
  }

  addToCart(itemId: number, quantity: number) {
    if(localStorage.getItem('currentUser') === null){
      alert('Please log in in order to add to cart');
      return;
    }
    if (quantity === 0){
      alert('Quantity is invalid!')
      return;
    }
    console.log(itemId);
    console.log(quantity);
    if (confirm("Item added!")) {
      this.cartServ.addToCart(this.customerId, itemId, quantity).subscribe(
        response => {
          console.log(response);
        }
      );
    } else {
      return;
    }

    location.reload();
  }

  ngOnInit(): void {
    this.getMenuItems();

    this.userServ.getCurrentUser(localStorage.getItem('currentUser')).subscribe(
      response => {
        console.log(response);
        this.cId = response.customerId;
      }
    );
  }

}
