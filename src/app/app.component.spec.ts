import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { Router } from '@Angular/router'
import { RouterTestingModule } from '@angular/router/testing'
import { HttpClientTestingModule} from '@angular/common/http/testing';
import { CommonModule } from '@angular/common';
import { AboutUsComponent } from './about-us/about-us.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { By } from '@angular/platform-browser';
import { CartComponent } from './cart/cart.component';
import { LoginComponent } from './login/login.component';
import { MenuComponent } from './menu/menu.component';
import { UserPortalComponent } from './user-portal/user-portal.component';
import {Location} from '@angular/common';
import { CartService } from './services/cart.service';





describe('AppComponent', () => {
  let router: Router;
  let location: Location;
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule,
        CommonModule,
        RouterTestingModule.withRoutes([
            {path: '', component: AppComponent},
            {path: 'welcome', component: WelcomeComponent},
            {path: 'menu', component: MenuComponent},
            {path: 'login', component: LoginComponent},
            {path: 'about-us', component: AboutUsComponent},
            {path: 'cart', component: CartComponent},
        ]),
      ],
      declarations: [
        AppComponent, WelcomeComponent, MenuComponent, LoginComponent, 
        AboutUsComponent, CartComponent, UserPortalComponent, 
      ],
/*      providers:[
      ], */

    }).compileComponents();
    fixture = TestBed.createComponent(AppComponent);
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    component = fixture.componentInstance;

    fixture.detectChanges;

  });

  it('should create the app', () => {
    // const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'roadkill-cafe-frontend'`, () => {
    // const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('roadkill-cafe-frontend');
  });

/*   it('should render title', () => {
    // const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelect('.content span').textContent).toContain('roadkill-cafe-frontend app is running!');
  }); */

  it('should go to About Us page',  waitForAsync(()=>{
    fixture.detectChanges();
    fixture.debugElement.query(By.css('#about-us')).nativeElement.click();

    fixture.whenStable().then(()=>{
        expect(location.path()).toEqual('/about-us');
      });
    
}));

it('should go to Menu page',  waitForAsync(()=>{
  fixture.detectChanges();
  fixture.debugElement.query(By.css('#menu')).nativeElement.click();

  fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/menu');
    });
  
}));

/* it('should go to Cart page',  waitForAsync(()=>{
  fixture.detectChanges();
  fixture.debugElement.query(By.css('#cart-icon')).nativeElement.click();

  fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/cart');
    });
  
})); */

it('should go to Welcom page',  waitForAsync(()=>{
  fixture.detectChanges();
  fixture.debugElement.query(By.css('#welcome')).nativeElement.click();

  fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/welcome');
    });
  
}));

it('should go to Login page',  waitForAsync(()=>{
  fixture.detectChanges();
  fixture.debugElement.query(By.css('#login-button')).nativeElement.click();

  fixture.whenStable().then(()=>{
      expect(location.path()).toEqual('/login');
    });
  
}));


});
