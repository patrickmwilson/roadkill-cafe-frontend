import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { By } from '@angular/platform-browser';
import { UserService } from '../services/user/user.service';
import { FormsModule } from '@angular/forms';

import { ForgotPasswordComponent } from './forgot-password.component';
import { Observable } from 'rxjs';

describe('ForgotPasswordComponent', () => {

  class MockService {
    recoverPassword(){}
  };

  let component: ForgotPasswordComponent;
  let fixture: ComponentFixture<ForgotPasswordComponent>;
  let router: Router;
  let userService: UserService;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy};

  /* beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ForgotPasswordComponent ]
    })
    .compileComponents();
  }); */

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([]),
        FormsModule,
      ],
      declarations: [ ForgotPasswordComponent ],
      providers:[
        {provide: UserService, useClass: MockService},
        {provide: HttpClient, useValue: mockClient}
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(ForgotPasswordComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.inject(Router);
    userService = TestBed.inject(UserService);
    mockClient = TestBed.get(HttpClient);

    component.loginEmail = "test@test.com";
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should have h3 say Recover Password', ()=>{
    fixture.detectChanges();
    let head = fixture.debugElement.query(By.css('h3')).nativeElement;
    expect(head.innerHTML).toBe('Recover Password');
  });

  it('should have instructions', ()=>{
    fixture.detectChanges();
    let para = fixture.debugElement.query(By.css('p')).nativeElement;
    expect(para.innerHTML).toBe("Enter the email address associated with your account and we'll email you a new password.");
  });

  it('should call the postRecoverPassword() method', waitForAsync(()=>{
    component.email = "test@test.com";
    fixture.detectChanges();
    let recoverButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;
    spyOn(component, 'postRecoverPassword');
    recoverButton.click();

    fixture.whenStable().then(()=>{
      expect(component.postRecoverPassword).toHaveBeenCalled();
    });
  }));

  it('should display an alert after calling the postRecoverPassword() method async', waitForAsync(()=>{
    component.email = "test@test.com";
    fixture.detectChanges();
    
    let userServiceSpy = spyOn(userService, 'recoverPassword').and.returnValue(Observable.create(observer=>{
      observer.next("Recovery email with your new password sent!");
    }));

    spyOn(window, 'alert');

    let recoverButton = fixture.debugElement.query(By.css('#submitbutton')).nativeElement;
    recoverButton.click();

    fixture.detectChanges();

    fixture.whenStable().then(()=>{
      expect(window.alert).toHaveBeenCalledWith('Recovery email with your new password sent!');
    });
  }));
});
