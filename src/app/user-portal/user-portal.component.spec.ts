import { HttpClient } from '@angular/common/http';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { Router } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { Observable } from 'rxjs';
import { ChangeInfoComponent } from '../change-info/change-info.component';
import { UserService } from '../services/user/user.service';
import { PastOrdersComponent } from '../view/past-orders/past-orders.component';

import { UserPortalComponent } from './user-portal.component';

describe('UserPortalComponent', () => {

  class MockService {
    getCurrentUser(){}
  }

  class MockGetUsername{
    getCurrentUsername() {
      return 'testuser';
    }
  }

  let dummyUserData = {
    customerId: 1,
    username: 'testuser',
    password: 'testpass',
    email: 'test@test.com',
    firstName: 'testfname', 
    lastName: 'testlastname',
    phoneNumber: '0001110000'
  };

  let component: UserPortalComponent;
  let fixture: ComponentFixture<UserPortalComponent>;
  let router: Router;
  let location: Location;
  let userService: UserService;
  let mockClient: {get: jasmine.Spy, post: jasmine.Spy, put: jasmine.Spy, delete: jasmine.Spy};


  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          {path: '', component: UserPortalComponent},
          {path: 'change-info', component: ChangeInfoComponent},
          {path: 'past-orders', component: PastOrdersComponent}
        ]),
      ],
      declarations: [ UserPortalComponent ],
      providers: [
        {provide: UserService, useClass: MockService},
        {provide: HttpClient, useValue: mockClient},
      ],
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserPortalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();

    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    userService = TestBed.inject(UserService);
    mockClient = TestBed.get(HttpClient);

    spyOn(component, 'getCurrentUsername').call(MockGetUsername);

    let userServiceSpy = spyOn(userService, 'getCurrentUser').and.returnValue(Observable.create(observer=>{
      observer.next(dummyUserData);
    }));

    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
